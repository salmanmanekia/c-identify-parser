//============================================================================
// Author      : Muhammad Salman
// Version     : 0.0.0.1
// Copyright   : Your copyright notice
//============================================================================

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

const unsigned int READ_BUFFER_SIZE = 512;
unsigned char read_buffer[READ_BUFFER_SIZE];

int main(int argc, char *argv[]) {

	static const char hex_chars[] = "0123456789ABCDEF";
	char model_number[40];
	char most_significant_ultra_dma_mode;
	bool smart_test_supported = false;

	ifstream src_file (argv[1],ios::binary);

	if (!src_file.is_open()) cout << "Error opening the file " << endl;

	else {
		while (src_file.read((char*)&read_buffer[0], READ_BUFFER_SIZE)) {
			// Get the number of bytes actually read.
			const unsigned int bytes_read = src_file.gcount();

			// Define the index and byte variables outside
			// of the loop to maybe save some execution time.
			unsigned char byte = 0;

			// For each byte that was read:
			for (unsigned int i = 0,j = 0; i < bytes_read; i++) {
				// Get source, binary value.
				byte = read_buffer[i];

				if ((i > 53) && (i < 94)) model_number[j++] = byte;

				if ((i == 175) && ((int)hex_chars[byte & 0x02] == 50)) smart_test_supported = true;

				if (i == 176) {
					most_significant_ultra_dma_mode = (int) hex_chars[(byte>>4)];
					if (most_significant_ultra_dma_mode & 4) cout << "Ultra DMA mode 6 is selected" << endl;
					else if (most_significant_ultra_dma_mode & 2) cout << "Ultra DMA mode 5 is selected" << endl;
					else if (most_significant_ultra_dma_mode & 1) cout << "Ultra DMA mode 4 is selected" << endl;
					else if ((int)hex_chars[byte & 0x08] == 56) cout << "Ultra DMA mode 3 is selected" << endl;
					else if ((int)hex_chars[byte & 0x04] == 52) cout << "Ultra DMA mode 2 is selected" << endl;
					else if ((int)hex_chars[byte & 0x02] == 50) cout << "Ultra DMA mode 1 is selected" << endl;
					else if ((int)hex_chars[byte & 0x01] == 49) cout << "Ultra DMA mode 0 is selected" << endl;
				}
			}
			cout << "Model Number : "<< model_number << endl;
			if (smart_test_supported)
				cout << "SMART self-test  supported " << endl;
			else
				cout << "SMART self-test not supported " << endl;

		}
	}
	src_file.close();
	return 0;
}
